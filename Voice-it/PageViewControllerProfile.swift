//
//  PageViewControllerProfile.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit

class PageViewControllerProfile: UIPageViewController {

    var pages: [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Ordered according to their orders in storyboard
        self.pages = ["Devices", "VoiceRecord", "Profile"]
        
        dataSource = self
        
        if let firstViewController = viewControllerAtIndex(2) {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: false,
                               completion: nil)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func viewControllerAtIndex(_ index: Int) -> UIViewController? {
        let storyboard = UIStoryboard(name: self.pages[index], bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: self.pages[index])
    }
    
}

extension PageViewControllerProfile: UIPageViewControllerDataSource {
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = self.pages.index(of: viewController.restorationIdentifier!) else {
            print("ERROR: Cannot find the index of current view controller")
            return nil
        }
        
        let previousIndex = (viewControllerIndex - 1)
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard self.pages.count > previousIndex else {
            return nil
        }
        
        return viewControllerAtIndex(previousIndex)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController,
                            viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = self.pages.index(of: viewController.restorationIdentifier!) else {
            print("ERROR: Cannot find the index of current view controller")
            return nil
        }
        
        let nextIndex = (viewControllerIndex + 1)
        
        guard self.pages.count != nextIndex else {
            return nil
        }
        guard self.pages.count > nextIndex else {
            return nil
        }
        
        return viewControllerAtIndex(nextIndex)
    }
    
}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
