//
//  AddingDeviceViewController.swift
//  Voice-it
//
//  Created by engincankurt on 18/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Alamofire
import FillableLoaders
import SwiftyBluetooth


class AddingDeviceViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var devicesName : [String] =  [String]()
    var selectedDevice : String = ""
    var selectedDeviceId : String = ""
    
    @IBOutlet weak var devicesTable: UITableView!
    @IBOutlet weak var refreshButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        SwiftyBluetooth.scanForPeripherals(withServiceUUIDs: nil, timeoutAfter: 15) { (PeripheralScanResult) in
            switch(PeripheralScanResult){
            case .scanStarted:
                print("Scanning is started")
                break
            case .scanResult(let peripheral, let advertisementData, let RSSI):
                print(peripheral.identifier)
                if peripheral.name != nil {
                    print("\(peripheral.name!)")
                    
                    if(!(self.devicesName.contains(peripheral.name!))){
                        self.selectedDeviceId = peripheral.identifier.uuidString
                        self.addElementToTable(peripheralName: peripheral.name!)
                    }
                }
                break
            case .scanStopped(let error):
                print("Scanning is finished.")
                
                if(self.devicesName.count == 0){
                    //Alert
                    let alert = UIAlertController(title: "No Devices", message: "There is no device around you.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    //Creating actions
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                    
                    //Adding actions.
                    alert.addAction(okAction)
                    
                    //Display alert.
                    self.present(alert, animated: true, completion: nil)
                }
                break
            }
        }
    }
    
    @IBAction func refreshButtonPressed(_ sender: Any) {
        
        SwiftyBluetooth.scanForPeripherals(withServiceUUIDs: nil, timeoutAfter: 15) { (PeripheralScanResult) in
            switch(PeripheralScanResult){
            case .scanStarted:
                print("Scanning is started")
                break
            case .scanResult(let peripheral, let advertisementData, let RSSI):
                if peripheral.name != nil {
                    print("\(peripheral.name!)")
                    print(peripheral.identifier)
                    if(!(self.devicesName.contains(peripheral.name!))){
                        self.selectedDeviceId = peripheral.identifier.uuidString
                        self.addElementToTable(peripheralName: peripheral.name!)
                    }
                }
                break
            case .scanStopped(let error):
                print("Scanning is finished.")
                break
            }
        }
    }
    
    @IBAction func addButtonPressed(_ sender: Any) {
        
        let urlDomain = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/add_account_to_device/"
        
        let parameters: Parameters = [ "account_id": "3",
                                       "device_id": self.selectedDeviceId
        ]
        
        Alamofire.request(urlDomain, method: .post, parameters: parameters).responseJSON { response in
            switch(response.result) {
            case .success(let data):
                let json = JSON(data)
                print(json)
                
            case .failure(_):
                print(response.result.error!)
                break
            }
        }
        
    }
    
    func addElementToTable(peripheralName: String) {
        self.devicesName.append(peripheralName)
        self.devicesTable.reloadData()
  
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return devicesName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell", for: indexPath) as UITableViewCell
        
        guard let deviceBLEName = cell.viewWithTag(1) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        guard let unselectedIcon = cell.viewWithTag(2) as? UIImageView else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        deviceBLEName.text = self.devicesName[indexPath.row]
        unselectedIcon.image = UIImage(named: "Circle Thin-96.png")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        
        guard let selectedName = cell.viewWithTag(1) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return
        }
        
        guard let selected = cell.viewWithTag(2) as? UIImageView else {
            print("ERROR: View with tag 2 is not the type it is casted for.")
            return
        }
                
        if(selectedDevice != "") { // Was previously selected
            // Remove selected one
            if(selectedDevice == self.devicesName[indexPath.row]){
                selectedDevice = ""

            }else{
                let alert = UIAlertController(title: "Selection", message: "You can choose only one device.", preferredStyle: UIAlertControllerStyle.alert)
                
                //Creating actions
                let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                
                //Adding actions.
                alert.addAction(okAction)
                
                //Display alert.
                self.present(alert, animated: true, completion: nil)
            }
            // Return to unselected state
            selected.image = UIImage(named: "Circle Thin-96.png")
            
        }else {// Was not previously selected
            // Set to device name
            selectedDevice = self.devicesName[indexPath.row]
            // Return to unselected state
            selected.image = UIImage(named: "Ok-96.png")
        }
        
        print(selectedDevice)
    }

    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")! as UITableViewCell
        
        if section == 0 {
            
            if let header = cell.viewWithTag(1) as? UILabel {
                header.text = "Select Voice-it"
                cell.backgroundColor = hexStringToUIColor(hex: "22008D")

            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
                
            }
            
        }
        return cell
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
