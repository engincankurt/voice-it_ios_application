//
//  CustomizationPeopleViewController.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Alamofire

class CustomizationPeopleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var urlDomain = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/devices/"
    let headers: HTTPHeaders = ["Authorization": "Token 393da8d2ac7183fd3a3eb781ad2cbb8f992b7cb8"]
    
    var usersNames = Array(repeating: Array(repeating: "", count: 10), count: 2)
    var usersIds = Array(repeating: Array(repeating: "", count: 10), count: 2)
    
    @IBOutlet weak var selectPeopleTable: UITableView!
    
    var selectedDeviceName : String!
    var selectedDeviceId : String!
    var sendingMessageData : NSData?
    var sendingFileURL: URL!
    
    var selectedNames : [String]!
    var selectedIds : [String]!
    
    var personCount = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.selectedNames = []
        self.selectedIds = []
        
        usersNames[1][0] = "Everyone"

        // Do any additional setup after loading the view, typically from a nib.
        urlDomain.append(selectedDeviceId)
        urlDomain.append("/")

        
        Alamofire.request(urlDomain, method: .get , parameters: nil, encoding: JSONEncoding.default, headers: headers) .responseJSON { response in
            
            switch(response.result) {
            case .success(let data):
                let json = JSON(data)
                print(json)
                var count = 1
                self.personCount = json["accounts"].count
                
                while self.personCount + 1 > count {
                    if(count == 1){
                        self.usersNames[1][count] = json["accounts"][count-1]["username"].stringValue
                        self.usersIds[1][count] = json["accounts"][count-1]["id"].stringValue
                        self.usersNames[0][count-1] = json["accounts"][count-1]["username"].stringValue
                        self.usersIds[0][count-1] = json["accounts"][count-1]["id"].stringValue
                        self.addElementToTable()
                    }else{
                        self.usersNames[1][count] = json["accounts"][count-1]["username"].stringValue
                        self.usersIds[1][count] = json["accounts"][count-1]["id"].stringValue
                        self.addElementToTable()
                    }
                    count += 1
                }
                
            case .failure(_):
                print(response.result.error!)
                break
            }
        }
    }

    func addElementToTable() {
        self.selectPeopleTable.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return personCount + 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectPeopleCell", for: indexPath) as UITableViewCell

        guard let personName = cell.viewWithTag(1) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        guard let profilePic = cell.viewWithTag(2) as? UIImageView else {
            print("ERROR: View with tag 2 is not the type it is casted for.")
            return cell
        }
        
        guard let isSelected = cell.viewWithTag(3) as? UIImageView else {
            print("ERROR: View with tag 3 is not the type it is casted for.")
            return cell
        }

        if indexPath.section == 0 && indexPath.row == 0 {
    
            personName.text = self.usersNames[0][indexPath.row]
            profilePic.isHidden = true
    
        }else{
            
            personName.text = self.usersNames[1][indexPath.row]
          
            if(indexPath.row != 0){
                profilePic.isHidden = true
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")! as UITableViewCell
        
        if section == 0 {
            
            if let typeOfPeople = cell.viewWithTag(1) as? UILabel {
                typeOfPeople.text = "Favorites"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")
                
            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let iconMostUsed = cell.viewWithTag(2) as? UIImageView else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
        } else {
            
            if let typeOfPeople = cell.viewWithTag(1) as? UILabel {
                typeOfPeople.text = "Everyone"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")
                
            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            if let iconMostUsed = cell.viewWithTag(2) as? UIImageView {
                iconMostUsed.isHidden = true
                
            }else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        
        guard let selectedName = cell.viewWithTag(1) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return
        }
        
        guard let selected = cell.viewWithTag(3) as? UIImageView else {
            print("ERROR: View with tag 3 is not the type it is casted for.")
            return
        }
        
        if(self.selectedNames.contains(self.usersNames[1][indexPath.row])) { // Was previously selected
            // Remove from selectedHobies
            if(indexPath.row == 0){
                while selectedNames.count > 0  {
                    self.selectedNames.removeFirst()
                    self.selectedIds.removeFirst()

                }
            }else if let index = selectedNames.index(of: self.usersNames[1][indexPath.row]) {
                selectedNames.remove(at: index)
                selectedIds.remove(at: index)
                selectedNames.removeLast()
                selectedIds.removeLast()
            }
            // Return to unselected state
            selected.image = UIImage(named: "Circle Thin-96.png")
        }else {// Was not previously selected
            // Add to selectedHobies
            var countElements = 0
            if(indexPath.row == 0){
                while selectedNames.count < personCount + 1 {
                    if(!self.selectedNames.contains(self.usersNames[1][countElements])) {
                        self.selectedNames.insert(self.usersNames[1][countElements], at: self.selectedNames.count)
                        self.selectedIds.insert(self.usersIds[1][countElements], at: self.selectedIds.count)
                    }
               
                    countElements += 1
                }
            selected.image = UIImage(named: "Ok-96.png")
            
            }else{
                
            self.selectedNames.insert(self.usersNames[1][indexPath.row], at: self.selectedNames.count)
            self.selectedIds.insert(self.usersIds[1][indexPath.row], at: self.selectedIds.count)
            // Return to unselected state
            selected.image = UIImage(named: "Ok-96.png")
                
            }
        }
        
        print(selectedNames)
        print(selectedIds)
        
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        if (segue.identifier == "peopleSegue") {
            let svc = segue!.destination as! CustomizationVariablesViewController;
            svc.selectedPeople = selectedNames
            svc.selectedPeopleIds = selectedIds
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "peopleBackSegue") {
            let svc = segue.destination as! CustomizationDeviceViewController;
            svc.fileURL = sendingFileURL
            svc.selectedDevice = selectedDeviceName
            svc.selectedId = selectedDeviceId
            
        }else if (segue.identifier == "peopleSegue") {
            let svc = segue.destination as! CustomizationVariablesViewController;
            svc.sentFileURL = sendingFileURL
            svc.selectedDeviceName = selectedDeviceName
            svc.selectedDeviceId = selectedDeviceId
            svc.selectedPeopleIds = selectedIds
        }

    }

    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
