//
//  LoginViewController.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Foundation
import Alamofire


class LoginViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    
    weak var currentTextFieldProfile: UITextField!
    
    var userToken: String = ""
    var defaultHeight: CGFloat = 0.0
    var keyboardY: CGFloat = 0.0
    var textY: CGFloat = 0.0
    var movement: CGFloat = 0.0;
    var times: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentTextFieldProfile = usernameTextField
        self.view.backgroundColor = (hexStringToUIColor(hex: "1B1464"))
        
        usernameTextField.layer.cornerRadius = 8.0
        usernameTextField.layer.masksToBounds = true
        usernameTextField.layer.borderColor = (hexStringToUIColor(hex: "FBC33B")).cgColor
        usernameTextField.layer.borderWidth = 3.0
        
        passwordTextField.layer.cornerRadius = 8.0
        passwordTextField.layer.masksToBounds = true
        passwordTextField.layer.borderColor = (hexStringToUIColor(hex: "FBC33B")).cgColor
        passwordTextField.layer.borderWidth = 3.0
        
        // Gesture setup
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(LoginViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    @IBAction func registrationButtonPressed(_ sender: Any) {
        
        dismissKeyboard()
        
        let storyboard:UIStoryboard = UIStoryboard.init(name: "Registration", bundle: nil)
        let vc:UIViewController = (storyboard.instantiateViewController(withIdentifier: "Registration"));
        vc.modalTransitionStyle =  UIModalTransitionStyle.crossDissolve
        self.present(vc, animated: true, completion: nil);
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        
        dismissKeyboard()
        
        let urlDomain = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/api-token-auth"
        
        let username = usernameTextField.text!
        let password = passwordTextField.text!
        
        let parameters: Parameters = [ "username": username,
                                       "password": password
                                     ]
        
        if username == "" || password == "" {
            let alert = UIAlertController(title: "Missing arguments", message: "You should enter your username and password.", preferredStyle: UIAlertControllerStyle.alert)

            //Creating actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            
            //Adding actions.
            alert.addAction(okAction)
            
            //Display alert.
            self.present(alert, animated: true, completion: nil)

        }else{
            
            Alamofire.request(urlDomain, method: .post, parameters: parameters).responseJSON { response in
                switch(response.result) {
                case .success(let data):
                    let json = JSON(data)
                    if response.result.value != nil{
                        print(json)
                        if json["token"].stringValue != "" {
                            
                            self.userToken = json["token"].stringValue
                            
                            //AppData singleton object
                            let sharedAppData = AppData.sharedAppData
                            
                            //Saving token
                            sharedAppData.setUserLoginToken(self.userToken)
                            
                            //Saving username
                            sharedAppData.setUsername(username)
                            
                            let storyboard:UIStoryboard = UIStoryboard.init(name: "VoiceRecord", bundle: nil)
                            let vc:UIViewController = (storyboard.instantiateViewController(withIdentifier: "VoiceRecord"));
                            vc.modalTransitionStyle =  UIModalTransitionStyle.crossDissolve
                            self.present(vc, animated: true, completion: nil);
                            
                        }else{
                        
                            let alert = UIAlertController(title: "Wrong arguments", message: "You should enter matching username and password.", preferredStyle: UIAlertControllerStyle.alert)
                            
                            //Creating actions
                            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                            
                            //Adding actions.
                            alert.addAction(okAction)
                            
                            //Display alert.
                            self.present(alert, animated: true, completion: nil)
                            
                        }
                    }
                    break
                    
                case .failure(_):
                    print(response.result.error!)
                    break
                }
            }
        }
    }
    
    func dismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }


    func keyboardWillShow(_ notif: Notification){
        times = times + 1
        if(times == 1){
            let info:NSDictionary = notif.userInfo! as NSDictionary;
            let kbFrame = info.object(forKey: UIKeyboardFrameEndUserInfoKey)
            let animationDuration = (info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as AnyObject).doubleValue
            let keyboardFrame = (kbFrame as AnyObject).cgRectValue
            let height = keyboardFrame?.size.height
            
            self.keyboardY = self.view.frame.height - height!
            self.textY = currentTextFieldProfile.frame.origin.y + currentTextFieldProfile.frame.height
            
            if(textY > keyboardY){
                movement = self.keyboardY - self.textY
            }else{
                movement = 0.0
            }
            
            UIView.beginAnimations("anim", context: nil);
            UIView.setAnimationBeginsFromCurrentState(true);
            UIView.setAnimationDuration(animationDuration!);
            self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement);
            UIView.commitAnimations();
            
        }
        
    }
    
    func keyboardWillHide(_ notif: Notification){
        let info:NSDictionary = notif.userInfo! as NSDictionary;
        let animationDuration = (info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as AnyObject).doubleValue
        
        if(self.textY > self.keyboardY){
            movement = textY - keyboardY
        }else{
            movement = 0.0
        }
        
        UIView.beginAnimations("anim", context: nil);
        UIView.setAnimationBeginsFromCurrentState(true);
        UIView.setAnimationDuration(animationDuration!);
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement);
        UIView.commitAnimations();
        
        times = 0
        
    }
    
    
    //  To navigate between text fields. Only through the further navigation is avaliable.
    //  Whenever the last text field is reached and done with editing it, next state is to signing up and
    //the button signing up is automatically called.
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        // Try to find next responder
        let nextTag = textField.tag + 1;
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder!
        
        if (nextResponder != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            //Not found, so remove keyboard
            textField.resignFirstResponder()
            loginButtonPressed(loginButton)

        }
        return false //Do not want UITextField to insert line-breaks.
    }
    
    //MARK: - Text Field Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextFieldProfile = textField
        //So that the right height or width values can be reached.
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextFieldProfile = textField
        //So that the right height or width values can be reached.
    }
    
    /*
     
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
}
