//
//  VoiceRecogViewController.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Foundation
import AmazonS3RequestManager

class VoiceRecogViewController: UIViewController, AVAudioPlayerDelegate, AVAudioRecorderDelegate {
    
    
    var audioPlayer: AVAudioPlayer?
    var audioRecorder: AVAudioRecorder?
    var error: NSError?
    var soundFileURL: URL?
    var soundFilePath: String = ""
    var messageFileURL: URL?
    var messageFilePath: String = ""
    var audioData : NSData?
    var amazons3Manager: AmazonS3RequestManager?
    var timer : Timer? = nil
    var count = 40

    @IBOutlet weak var startRocordButton: UIButton!
    @IBOutlet weak var stopRecordButton: UIButton!
    @IBOutlet weak var playRecordButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var countdownLabel: UILabel!
    @IBOutlet weak var checkmark: UIImageView!

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        playRecordButton.isEnabled = false
        stopRecordButton.isEnabled = false
        
        countdownLabel.text = String(count)
        checkmark.isHidden = false
        
        amazons3Manager = AmazonS3RequestManager(bucket: "voiceitbucket",
                                                 region: .EUCentral1,
                                                 accessKey: "AKIAIFQ4EV4GUFOWMIOQ",
                                                 secret: "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")
    
        let dirPaths =
            NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                .userDomainMask, true)
        
        let docsDir = dirPaths[0]
        soundFilePath = (docsDir as NSString).appendingPathComponent("sound.wav")
        soundFileURL = URL(fileURLWithPath: soundFilePath)

        let recordSettings = [AVEncoderAudioQualityKey: AVAudioQuality.min.rawValue,
                              AVEncoderBitRateKey: 16,
                              AVNumberOfChannelsKey: 2,
                              AVSampleRateKey: 44100.0] as [String : Any]
        
        let audioSession = AVAudioSession.sharedInstance()
        try! audioSession.setCategory(AVAudioSessionCategoryPlayAndRecord, with: [])
        try! audioSession.setActive(true)
        try! audioSession.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)

        
        if let err = error {
            print("audioSession error: \(err.localizedDescription)")
        }
        
        do {
            audioRecorder = try AVAudioRecorder(url: soundFileURL!,
                                                settings: recordSettings as [String : AnyObject])
        } catch {
            audioRecorder = nil
        }
        
        audioRecorder?.prepareToRecord()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func startRecord(_ sender: AnyObject) {
        if audioRecorder?.isRecording == false {
            playRecordButton.isEnabled = false
            startRocordButton.isEnabled = false
            stopRecordButton.isEnabled = true
            audioRecorder?.record()
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(VoiceRecogViewController.update), userInfo: nil, repeats: true)
        }
    }
    
    @IBAction func stopRecord(_ sender: AnyObject) {
        stopRecordButton.isEnabled = false
        playRecordButton.isEnabled = true
        startRocordButton.isEnabled = true
        
        if audioRecorder?.isRecording == true {
            audioRecorder?.stop()
            timer?.invalidate()
            count=40
        } else {
            audioPlayer?.stop()
        }
    }
    
    @IBAction func playRecord(_ sender: AnyObject) {
        if audioRecorder?.isRecording == false {
            stopRecordButton.isEnabled = true
            startRocordButton.isEnabled = false
        }
        
        let dirPaths =
            NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                .userDomainMask, true)

        let messagesDir = dirPaths[0]
        messageFilePath = (messagesDir as NSString).appendingPathComponent("test6.wav")
        messageFileURL = URL(fileURLWithPath: messageFilePath)
        
        let destination: DownloadRequest.DownloadFileDestination = { _, _ in
            return (self.messageFileURL!, [.createIntermediateDirectories, .removePreviousFile])
        }

        amazons3Manager?.download(at: "https://s3.eu-central-1.amazonaws.com/voiceitbucket/voice-messages/test.wav", to: destination).responseData { response in
            
            print(response)
            
            if let data = response.result.value {
                
                print(data)
                
            } else {
                
            }
        }
    
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: soundFileURL!)
            audioPlayer?.delegate = self
            audioPlayer?.prepareToPlay()
            audioPlayer?.play()
        } catch {
            print("audioPlayer error")
        }
    }
    
    func update() {
        if(count > 0) {
            count -= 1
            countdownLabel.text = String(count)
        }
    }

    @IBAction func continueRegist(_ sender: AnyObject) {
        
        let headers: HTTPHeaders = ["Authorization": "Token 393da8d2ac7183fd3a3eb781ad2cbb8f992b7cb8"]
        audioData = NSData (contentsOf: soundFileURL!)
        let URL = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/upload/voice-messages"
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append("3".data(using: String.Encoding.utf8)!, withName: "to_device_id")
            multipartFormData.append("3".data(using: String.Encoding.utf8)!, withName: "from_account_id")
            multipartFormData.append("55".data(using: String.Encoding.utf8)!, withName: "to_account_ids")
            multipartFormData.append(self.audioData as! Data, withName: "file", fileName: "file.wav", mimeType: "audio/wav")

        },usingThreshold: UInt64.init(), to: URL, method: .put, headers: headers,
                         encodingCompletion: { encodingResult in
                            switch encodingResult {
                            case .success(let upload, _, _):
                                upload.responseJSON { response in
                                    debugPrint(response)
                                }
                            case .failure(let encodingError):
                                print(encodingError)
                            }
        })
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        startRocordButton.isEnabled = true
        stopRecordButton.isEnabled = false
    }
    
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("Audio Play Decode Error")
    }
    
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool) {
    }
    
    func audioRecorderEncodeErrorDidOccur(_ recorder: AVAudioRecorder, error: Error?) {
        print("Audio Record Encode Error")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
}
