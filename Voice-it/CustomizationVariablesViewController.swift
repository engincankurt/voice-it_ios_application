//
//  CustomizationVariablesViewController.swift
//  Voice-it
//
//  Created by engincankurt on 04/12/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Alamofire
import DateTimePicker

class CustomizationVariablesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    static var localTimeZoneAbbreviation: String { return  NSTimeZone.local.abbreviation(for: Date())! }
    let currentDate = Date()

    @IBOutlet weak var customizationTable: UITableView!
    
    var selectedPeople : [String]!
    
    var setTime1 = 0
    var setTime2 = 0
    
    
    var selectedDeviceName : String!
    var selectedDeviceId : String!
    var sentMessageData : NSData?
    var selectedPeopleIds : [String]!
    var sentFileURL: URL?
    

    var will_repeat_by_motion : Bool = false
    var repeat_time : String! = ""
    var expireDate : String = ""
    var date : String = ""
    var will_repeat_by_time : Bool = false
    var will_repeat : Bool = false
    var is_set_time : Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
       
        print(selectedDeviceName)
        print(currentDate)

        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func setTimeChoosed(_ sender: Any) {
        
        setTime1 += 1
       
        if(setTime1 % 2 == 1){
            let picker = DateTimePicker.show()
            picker.highlightColor = hexStringToUIColor(hex: "FBC33B")
            
            picker.completionHandler = { date in
                if(date.compare(self.currentDate).rawValue > 0){
                    self.date = "\(date)"
                    self.is_set_time = true
                    self.customizationTable.reloadData()
                    
                    print(date)
                }else{
                    
                    let alert = UIAlertController(title: "Invalid time", message: "You should choose an valid time.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    //Creating actions
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                    
                    //Adding actions.
                    alert.addAction(okAction)
                    
                    //Display alert.
                    self.present(alert, animated: true, completion: nil)
                    
                    print(date)
                }
                
            }
        }else{
            self.date =  ""
            self.is_set_time = false
            self.customizationTable.reloadData()
        }
    }
        
    @IBAction func setTimeChoosed2(_ sender: Any) {
      
        setTime2 += 1
        
        if(setTime2 % 2 == 1){
            let picker = DateTimePicker.show()
            picker.highlightColor = hexStringToUIColor(hex: "FBC33B")
            
            picker.completionHandler = { date in

                if(date.compare(self.currentDate).rawValue > 0){
                    self.expireDate = "\(date)"
                    self.customizationTable.reloadData()

                    print(date)
                }else{
                    
                    let alert = UIAlertController(title: "Invalid time", message: "You should choose an valid time.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    //Creating actions
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                    
                    //Adding actions.
                    alert.addAction(okAction)
                    
                    //Display alert.
                    self.present(alert, animated: true, completion: nil)
                    
                    print(date)
                }
                
            }
        }else{
            self.expireDate =  ""
            self.customizationTable.reloadData()
        }
    }
    
    @IBAction func sendButtonPressed(_ sender: Any) {
        
        let headers: HTTPHeaders = ["Authorization": "Token 393da8d2ac7183fd3a3eb781ad2cbb8f992b7cb8"]
        var URL = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages/with-options?"
       
        let index1 = self.date.index(self.date.endIndex, offsetBy: -6)
        self.date = self.date.substring(to: index1)
        self.date = self.date.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)
        
        if(date != ""){
            URL.append("set_date=\(date)")
        }
        
        let index2 = self.expireDate.index(expireDate.endIndex, offsetBy: -6)
        self.expireDate = self.expireDate.substring(to: index2)
        self.expireDate = self.expireDate.replacingOccurrences(of: " ", with: "_", options: .literal, range: nil)

        if(expireDate != ""){
            URL.append("&expiration_date=\(expireDate)")
        }
        
        var selectedPeoples : String = ""
        selectedPeoples.append("[\((selectedPeopleIds.last?.description)!)")
        selectedPeopleIds.removeLast()
        while(selectedPeopleIds.count != 0){
            selectedPeoples.append(",\((selectedPeopleIds.last?.description)!)")
            selectedPeopleIds.removeLast()
        }
        selectedPeoples.append("]")
        
        sentMessageData = NSData (contentsOf: sentFileURL!)

        Alamofire.upload(multipartFormData: { (multipartFormData) in
            multipartFormData.append("3".data(using: String.Encoding.utf8)!, withName: "from_account_id")
            multipartFormData.append(self.selectedDeviceId.data(using: String.Encoding.utf8)!, withName: "to_device_id")
            multipartFormData.append(selectedPeoples.data(using: String.Encoding.utf8)!, withName: "to_account_ids")
            multipartFormData.append(self.sentMessageData! as Data, withName: "file", fileName: "file.wav", mimeType: "audio/wav")
            
        },usingThreshold: UInt64.init(), to: URL, method: .post, headers: headers,
          encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                upload.responseString { response in
                    print(response)
                }
            case .failure(let encodingError):
                print(encodingError)
            }
        })
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 2
        }
        else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 && indexPath.row == 0 {
          
            let cell = tableView.dequeueReusableCell(withIdentifier: "Section0Cell1", for: indexPath) as UITableViewCell
            
            guard let type = cell.viewWithTag(1) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let date = cell.viewWithTag(3) as? UILabel else {
                print("ERROR: View with tag 3 is not the type it is casted for.")
                return cell
            }
           
            type.text = "Time"
            
            if(self.date != ""){
                let index = self.date.index(self.date.endIndex, offsetBy: -5)
                date.text = self.date.substring(to: index)
            }else{
                date.text = self.date
            }
            
            return cell
        
        }else if indexPath.section == 0 && indexPath.row == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Section0Cell2", for: indexPath) as UITableViewCell
            
            guard let type = cell.viewWithTag(1) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let date = cell.viewWithTag(3) as? UILabel else {
                print("ERROR: View with tag 3 is not the type it is casted for.")
                return cell
            }
            
            type.text = "Expired Time"
            
            if(self.expireDate != ""){
                let index = self.expireDate.index(self.expireDate.endIndex, offsetBy: -5)
                date.text = self.expireDate.substring(to: index)
            }else{
                date.text = self.expireDate
            }
            
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "Section1Cell", for: indexPath) as UITableViewCell

            guard let date = cell.viewWithTag(2) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            date.text = ""
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if(indexPath.section == 0){
            return 55.0
        }else{
            return 84.0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")! as UITableViewCell
        
        if section == 0 {
            
            if let typeOfPeople = cell.viewWithTag(1) as? UILabel {
                typeOfPeople.text = "Time Preferences"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")

                
            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let image = cell.viewWithTag(2) as? UIImageView else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
        } else {
            
            if let typeOfPeople = cell.viewWithTag(1) as? UILabel {
                typeOfPeople.text = "Repetition"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")
                
            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            if let image = cell.viewWithTag(2) as? UIImageView {
                image.image = UIImage(named: "Repeat-104.png")
                
            }else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
        }
        
        return cell
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "variableBackSegue") {
            let svc = segue.destination as! CustomizationPeopleViewController;
            svc.sendingFileURL = sentFileURL
            svc.selectedDeviceName = selectedDeviceName
            svc.selectedDeviceId = selectedDeviceId
            svc.selectedIds = selectedPeopleIds
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
