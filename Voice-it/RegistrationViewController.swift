//
//  RegistrationViewController.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class RegistrationViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    weak var currentTextField: UITextField!
    
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!
    
    var userToken: String = ""
    var defaultHeight: CGFloat = 0.0
    var keyboardY: CGFloat = 0.0
    var textY: CGFloat = 0.0
    var movement: CGFloat = 0.0;
    var times: Int = 0
    
    let lowerCase = NSCharacterSet.lowercaseLetters
    let upperCase = NSCharacterSet.uppercaseLetters
    let symbols = NSCharacterSet.alphanumerics.inverted
    let digits = NSCharacterSet.decimalDigits
    
    override func viewDidLoad() {
        super.viewDidLoad()
        currentTextField = nameTextField
        
        nameTextField.layer.cornerRadius = 8.0
        nameTextField.layer.masksToBounds = true
        nameTextField.layer.borderColor = (hexStringToUIColor(hex: "FBC33B")).cgColor
        nameTextField.layer.borderWidth = 3.0
        
        surnameTextField.layer.cornerRadius = 8.0
        surnameTextField.layer.masksToBounds = true
        surnameTextField.layer.borderColor = (hexStringToUIColor(hex: "FBC33B")).cgColor
        surnameTextField.layer.borderWidth = 3.0
        
        usernameTextField.layer.cornerRadius = 8.0
        usernameTextField.layer.masksToBounds = true
        usernameTextField.layer.borderColor = (hexStringToUIColor(hex: "FBC33B")).cgColor
        usernameTextField.layer.borderWidth = 3.0
        
        mailTextField.layer.cornerRadius = 8.0
        mailTextField.layer.masksToBounds = true
        mailTextField.layer.borderColor = (hexStringToUIColor(hex: "FBC33B")).cgColor
        mailTextField.layer.borderWidth = 3.0
        
        passwordTextField.layer.cornerRadius = 8.0
        passwordTextField.layer.masksToBounds = true
        passwordTextField.layer.borderColor = (hexStringToUIColor(hex: "FBC33B")).cgColor
        passwordTextField.layer.borderWidth = 3.0
            
        // Gesture setup
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistrationViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillShow(_:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(RegistrationViewController.keyboardWillHide(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return UIStatusBarStyle.lightContent
    }
    
    @IBAction func continueButtonPressed(_ sender: Any) {
        
        dismissKeyboard()
        
        let urlDomain = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/users/"
        
        let name = nameTextField.text!
        let surname = surnameTextField.text!
        let username = usernameTextField.text!
        let email = mailTextField.text!
        let password = passwordTextField.text!
        
        if name == "" || surname == "" || username == "" || email == "" || password == "" {
            
            let alert = UIAlertController(title: "Missing arguments", message: "You should enter all necessary arguments.", preferredStyle: UIAlertControllerStyle.alert)
            
            //Creating actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            
            //Adding actions.
            alert.addAction(okAction)
            
            //Display alert.
            self.present(alert, animated: true, completion: nil)
            
            
        }else if password.characters.count<6 || password.characters.count>20 {
            
            //Alert
            let alert = UIAlertController(title: "Password Length", message: "Password should be at least 6 characters", preferredStyle: UIAlertControllerStyle.alert)
            
            //Creating actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            
            //Adding actions.
            alert.addAction(okAction)
            
            //Display alert.
            self.present(alert, animated: true, completion: nil)

            
        }else if password.rangeOfCharacter(from: digits) == nil {

            //Alert
            let alert = UIAlertController(title: "Password Error", message: "Password should contain at least a number", preferredStyle: UIAlertControllerStyle.alert)
            
            //Creating actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            
            //Adding actions.
            alert.addAction(okAction)
            
            //Display alert.
            self.present(alert, animated: true, completion: nil)

            
        }else if password.rangeOfCharacter(from: upperCase)  == nil {

            //Alert
            let alert = UIAlertController(title: "Password Error", message: "Password should contain an uppercase letter", preferredStyle: UIAlertControllerStyle.alert)
            
            //Creating actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            
            //Adding actions.
            alert.addAction(okAction)
            
            //Display alert.
            self.present(alert, animated: true, completion: nil)

            
        }else if password.rangeOfCharacter(from: lowerCase)  == nil {

            //Alert
            let alert = UIAlertController(title: "Password Error", message: "Password should contain an lowercase letter", preferredStyle: UIAlertControllerStyle.alert)
            
                    //Creating actions
            let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
            
                    //Adding actions.
            alert.addAction(okAction)
            
                //Display alert.
            self.present(alert, animated: true, completion: nil)
            
        }else{
        
        let parameters: Parameters = [ "username": username,
                                       "first_name": name,
                                       "last_name": surname,
                                       "email": email,
                                       "password": password
                                     ]
        
        Alamofire.request(urlDomain, method: .post, parameters: parameters).responseJSON { response in
            
            switch(response.result) {
            case .success(let data):
                let json = JSON(data)
                if response.result.value != nil{
                    print(json)
                    if json["account"]["id"].stringValue != "" {
                        print(response.result.value!)
                        let storyboard:UIStoryboard = UIStoryboard.init(name: "VoiceRecog", bundle: nil)
                        let vc:UIViewController = (storyboard.instantiateViewController(withIdentifier: "VoiceRecog"));
                        
                        vc.modalTransitionStyle =  UIModalTransitionStyle.crossDissolve
                        self.present(vc, animated: true, completion: nil);
                        
                        let loginParameters: Parameters = [ "username": username,
                                                            "password": password
                        ]
                        let urlDomainLogin = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/api-token-auth"
                        
                        Alamofire.request(urlDomainLogin, method: .post, parameters: loginParameters).responseJSON { response in
                            switch(response.result) {
                            case .success(let data):
                                let json = JSON(data)
                                if response.result.value != nil{
                                    print(json)
                                    if json["token"].stringValue != "" {
                                        self.userToken = json["token"].stringValue
                                    }
                                }
                                break
                                
                            case .failure(_):
                                print(response.result.error!)
                                break
                            }
                        }
                    
                    }else if json["username"][0].stringValue == "A user with that username already exists." {

                        //Alert
                        let alert = UIAlertController(title: "Username Error", message: "A user with that username already exists", preferredStyle: UIAlertControllerStyle.alert)
                        
                        //Creating actions
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                        
                        //Adding actions.
                        alert.addAction(okAction)
                        
                        //Display alert.
                        self.present(alert, animated: true, completion: nil)
                        break
                    
                    }else if json["email"][0].stringValue == "Enter a valid email address." {
                        //Alert
                        let alert = UIAlertController(title: "Email Error", message: "Enter a valid email address", preferredStyle: UIAlertControllerStyle.alert)
                        
                        //Creating actions
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                        
                        //Adding actions.
                        alert.addAction(okAction)
                        
                        //Display alert.
                        self.present(alert, animated: true, completion: nil)
                        break
                    }else{
                        let alert = UIAlertController(title: "Error", message: "Unknown situation", preferredStyle: UIAlertControllerStyle.alert)
                        
                        //Creating actions
                        let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                        
                        //Adding actions.
                        alert.addAction(okAction)
                        
                        //Display alert.
                        self.present(alert, animated: true, completion: nil)
                        break
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error!)
                break
                
              }
            }
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: AnyObject){
        //Causes the view to resign the first responder status.
        dismiss(animated: true, completion: nil);
    }
    
    func dismissKeyboard(){
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func keyboardWillShow(_ notif: Notification){
        times = times + 1
        if(times == 1){
            let info:NSDictionary = notif.userInfo! as NSDictionary;
            let kbFrame = info.object(forKey: UIKeyboardFrameEndUserInfoKey)
            let animationDuration = (info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as AnyObject).doubleValue
            let keyboardFrame = (kbFrame as AnyObject).cgRectValue
            let height = keyboardFrame?.size.height
            
            self.keyboardY = (self.view.frame.height - height!)
            self.textY = currentTextField.frame.origin.y + currentTextField.frame.height
        
            if(textY > keyboardY){
                movement = self.keyboardY - self.textY
            }else{
                movement = 0.0
            }
            
            UIView.beginAnimations("anim", context: nil);
            UIView.setAnimationBeginsFromCurrentState(true);
            UIView.setAnimationDuration(animationDuration!);
            self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement);
            UIView.commitAnimations();
            
            cancelButton.isHidden = true
        }
        
    }
    
    //When touch upon any place in view, keyboard will be dismissed.
    func keyboardWillHide(_ notif: Notification){
        let info:NSDictionary = notif.userInfo! as NSDictionary;
        let animationDuration = (info.object(forKey: UIKeyboardAnimationDurationUserInfoKey) as AnyObject).doubleValue
        
        if(self.textY > self.keyboardY){
            movement = textY - keyboardY
        }else{
            movement = 0.0
        }
        
        
        UIView.beginAnimations("anim", context: nil);
        UIView.setAnimationBeginsFromCurrentState(true);
        UIView.setAnimationDuration(animationDuration!);
        self.view.frame = self.view.frame.offsetBy(dx: 0,  dy: movement);
        UIView.commitAnimations();
        
        times = 0
        
        cancelButton.isHidden = false
    }
    
    //  To navigate between text fields. Only through the further navigation is avaliable.
    //  Whenever the last text field is reached and done with editing it, next state is to signing up and
    //the button signing up is automatically called.
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        // Try to find next responder
        let nextTag = textField.tag + 1;
        let nextResponder = textField.superview?.viewWithTag(nextTag) as UIResponder!
        
        if (nextResponder != nil){
            // Found next responder, so set it.
            nextResponder?.becomeFirstResponder()
        }
        else
        {
            //Not found, so remove keyboard
            textField.resignFirstResponder()
            
            continueButtonPressed(continueButton)
        }
        return false //Do not want UITextField to insert line-breaks.
    }
        
    //MARK: - Text Field Delegate
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        currentTextField = textField
        //So that the right height or width values can be reached.
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        currentTextField = textField
        //So that the right height or width values can be reached.
    }
    
     /*
         
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
}
