//
//  DevicesViewController.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Alamofire

class DevicesViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let urlUsers = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/devices/bound_to/3"
    let headers: HTTPHeaders = ["Authorization": "Token 393da8d2ac7183fd3a3eb781ad2cbb8f992b7cb8"]

    var devicesNames = Array(repeating: Array(repeating: "", count: 10), count: 2)
    var usersNames = Array(repeating: Array(repeating: "", count: 10), count: 10)
    
    var totalDevice = 0

    @IBOutlet weak var deviceTable: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request(urlUsers, method: .get , parameters: nil, encoding: JSONEncoding.default, headers: headers) .responseJSON { response in
            
            switch(response.result) {
            case .success(let data):
                let json = JSON(data)
                print(json)
                var countDevice = 0
                var countUser = 0
                self.totalDevice = json.count
                
                while self.totalDevice > countDevice {
                    self.devicesNames[1][countDevice] = json[countDevice]["name"].stringValue
                    self.addElementToTable()
                    
                    if json[countDevice]["accounts"] != "" {
                        
                        var deviceUsers = json[countDevice]["accounts"].count

                        while deviceUsers > countUser {
                            
                            self.usersNames[countDevice][countUser] = json[countDevice]["accounts"][countUser]["username"].stringValue
                            self.addElementToTable()
                            countUser += 1
                        }
                    }
                    
                    countDevice += 1
                    countUser = 0
                }
                
                if json.count == 0 {
                    
                    let alert = UIAlertController(title: "No Device", message: "You do not have any device, you can add by clicking plus button", preferredStyle: UIAlertControllerStyle.alert)
                    
                    //Creating actions
                    let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                    
                    //Adding actions.
                    alert.addAction(okAction)
                    
                    //Display alert.
                    self.present(alert, animated: true, completion: nil)
                    break
                }

            case .failure(_):
                print(response.result.error!)
                
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return totalDevice
        }
    }
    
    @IBAction func addUserButtonPressed(_ sender: Any) {
    }
    
    @IBAction func settingsButtonPressed(_ sender: Any) {
    }
    
    @IBAction func deleteButtonPressed(_ sender: Any) {
    }
    
    func addElementToTable() {
        self.deviceTable.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DeviceCell", for: indexPath) as UITableViewCell
        
        guard let deviceName = cell.viewWithTag(1) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            
            return cell
        }
        
        guard let profilePic = cell.viewWithTag(2) as? UIImageView else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        guard let personName = cell.viewWithTag(3) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        guard let addUserButton = cell.viewWithTag(4) as? UIButton else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        guard let settingsButton = cell.viewWithTag(5) as? UIButton else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        guard let deleteButton = cell.viewWithTag(6) as? UIButton else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }
        
        if indexPath.section == 0 {
        
        } else {
            deviceName.text = self.devicesNames[1][indexPath.row]

        }
        
        profilePic.layer.cornerRadius = profilePic.frame.size.width / 2
        profilePic.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")! as UITableViewCell
        
        if section == 0 {
            
            if let typeOfDevices = cell.viewWithTag(1) as? UILabel {
                typeOfDevices.text = "Most Used"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")

            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let iconMostUsed = cell.viewWithTag(2) as? UIImageView else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
        } else {
            
            if let typeOfDevices = cell.viewWithTag(1) as? UILabel {
                typeOfDevices.text = "All Devices"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")
                
            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            if let iconMostUsed = cell.viewWithTag(2) as? UIImageView {
                iconMostUsed.isHidden = true
           
            }else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
        }
        
        return cell
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
  
//       override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
//        if (segue.identifier == "DeviceUsers") {
//            guard let destinationViewController = segue.destination as? DeviceUsersViewController else {
//                print("ERROR: Destination is not a valid view controller.")
//                return
//            }
//            var indexPath = self.deviceTable.indexPathForSelectedRow
//            destinationViewController.users = usersNames[indexPath]
//        }
//     }
}
