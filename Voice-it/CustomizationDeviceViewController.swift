//
//  CustomizationDeviceViewController.swift
//  Voice-it
//
//  Created by engincankurt on 04/12/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Alamofire

class CustomizationDeviceViewController:UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var selectDeviceTable: UITableView!
    
    let urlUsers = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/devices/bound_to/3"
    let headers: HTTPHeaders = ["Authorization": "Token 393da8d2ac7183fd3a3eb781ad2cbb8f992b7cb8"]
    
    var devicesNames = Array(repeating: Array(repeating: "", count: 10), count: 2)
    var devicesIds = Array(repeating: Array(repeating: "", count: 10), count: 2)
    var totalDevice = 0
    
    var selectedDevice : String = ""
    var fileURL: URL?
    
    var selectedId : String = ""
    var messageData : NSData?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        Alamofire.request(urlUsers, method: .get , parameters: nil, encoding: JSONEncoding.default, headers: headers) .responseJSON { response in
            
            switch(response.result) {
            case .success(let data):
                let json = JSON(data)
                print(json)
                var countDevice = 0
                self.totalDevice = json.count
                
                while self.totalDevice > countDevice {
                    if(countDevice == 0){
                        self.devicesNames[1][countDevice] = json[countDevice]["name"].stringValue
                        self.devicesIds[1][countDevice] = json[countDevice]["id"].stringValue
                        self.devicesNames[0][countDevice] = json[countDevice]["name"].stringValue
                        self.devicesIds[0][countDevice] = json[countDevice]["id"].stringValue
                        self.addElementToTable()
                        
                        countDevice += 1
                    }else{
                        self.devicesNames[1][countDevice] = json[countDevice]["name"].stringValue
                        self.devicesIds[1][countDevice] = json[countDevice]["id"].stringValue
                        self.addElementToTable()
                        
                        countDevice += 1
                        
                    }
                }
                
            case .failure(_):
                print(response.result.error!)
                
            }
        }

        // Do any additional setup after loading the view.
    }
    
    func addElementToTable() {
        self.selectDeviceTable.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        else {
            return totalDevice
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDeviceCell", for: indexPath) as UITableViewCell
        
        
        guard let deviceName = cell.viewWithTag(1) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return cell
        }

        if indexPath.section == 0 && indexPath.row == 0  {
            
            deviceName.text = self.devicesNames[0][indexPath.row]
            
        }else{
           
            deviceName.text = self.devicesNames[1][indexPath.row]
        
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderCell")! as UITableViewCell
        
        if section == 0 {
            
            if let typeOfDevices = cell.viewWithTag(1) as? UILabel {
                typeOfDevices.text = "Most Used"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")
                
            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let iconMostUsed = cell.viewWithTag(2) as? UIImageView else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
        } else {
            
            if let typeOfDevices = cell.viewWithTag(1) as? UILabel {
                typeOfDevices.text = "All Devices"
                cell.backgroundColor = hexStringToUIColor(hex: "1B1464")
                
            } else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            if let iconMostUsed = cell.viewWithTag(2) as? UIImageView {
                iconMostUsed.isHidden = true
                
            }else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        
        guard let selectedName = cell.viewWithTag(1) as? UILabel else {
            print("ERROR: View with tag 1 is not the type it is casted for.")
            return
        }
        
        guard let selected = cell.viewWithTag(3) as? UIImageView else {
            print("ERROR: View with tag 3 is not the type it is casted for.")
            return
        }
        
        if(selectedDevice != "") { // Was previously selected
            // Remove selected one
            if(selectedDevice == self.devicesNames[1][indexPath.row]){
                
                if(indexPath.row == 0 && indexPath.section == 0){
                    selectedDevice = ""
                    selected.image = UIImage(named: "Circle Thin-96.png")
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDeviceCell", for: IndexPath(row: 0, section: 1)) as UITableViewCell
                    
                    guard let selected2 = cell.viewWithTag(3) as? UIImageView else {
                        print("ERROR: View with tag 3 is not the type it is casted for.")
                        return
                    }
                    
                    selected2.image = UIImage(named: "Circle Thin-96.png")
                    self.selectDeviceTable.reloadData()

                
                }else if(indexPath.row == 0 && indexPath.section == 1){
                    selectedDevice = ""
                    selected.image = UIImage(named: "Circle Thin-96.png")
                    
                    let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDeviceCell", for: IndexPath(row: 0, section: 0)) as UITableViewCell
                    
                    guard let selected2 = cell.viewWithTag(3) as? UIImageView else {
                        print("ERROR: View with tag 3 is not the type it is casted for.")
                        return
                    }
                    
                    selected2.image = UIImage(named: "Circle Thin-96.png")
                    self.selectDeviceTable.reloadData()

                
                }else{
                    selectedDevice = ""
                    selected.image = UIImage(named: "Circle Thin-96.png")
                    self.selectDeviceTable.reloadData()

                }

            
            }else{
                let alert = UIAlertController(title: "Selection", message: "You can choose only one device.", preferredStyle: UIAlertControllerStyle.alert)

                //Creating actions
                let okAction = UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil)
                
                //Adding actions.
                alert.addAction(okAction)
                
                //Display alert.
                self.present(alert, animated: true, completion: nil)
            }
            
            
        }else {// Was not previously selected
            // Set to device name
            if(indexPath.row == 0 && indexPath.section == 0){
                
                selectedDevice = self.devicesNames[1][indexPath.row]
                selectedId = self.devicesIds[1][indexPath.row]
                // Return to unselected state
                selected.image = UIImage(named: "Ok-96.png")
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDeviceCell", for: IndexPath(row: 0, section: 1)) as UITableViewCell
                
                guard let selected2 = cell.viewWithTag(3) as? UIImageView else {
                    print("ERROR: View with tag 3 is not the type it is casted for.")
                    return
                }
                
                selected2.image = UIImage(named: "Ok-96.png")
                self.selectDeviceTable.reloadData()

            
            }else if(indexPath.row == 0 && indexPath.section == 1){
                
                selectedDevice = self.devicesNames[1][indexPath.row]
                selectedId = self.devicesIds[1][indexPath.row]
                // Return to unselected state
                selected.image = UIImage(named: "Ok-96.png")
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "SelectDeviceCell", for: IndexPath(row: 0, section: 0)) as UITableViewCell
                
                guard let selected2 = cell.viewWithTag(3) as? UIImageView else {
                    print("ERROR: View with tag 3 is not the type it is casted for.")
                    return
                }
                
                selected2.image = UIImage(named: "Ok-96.png")
                self.selectDeviceTable.reloadData()
                
            }else{
                selectedDevice = self.devicesNames[1][indexPath.row]
                selectedId = self.devicesIds[1][indexPath.row]
                // Return to unselected state
                selected.image = UIImage(named: "Ok-96.png")
                self.selectDeviceTable.reloadData()

            }
        }

        print(selectedDevice)
        print(selectedId)

    }

    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any!) {
        if (segue.identifier == "deviceSegue") {
            let svc = segue.destination as! CustomizationPeopleViewController;
            svc.sendingFileURL = fileURL
            svc.selectedDeviceName = selectedDevice
            svc.selectedDeviceId = selectedId
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
