//
//  ProfilePageViewController.swift
//  Voice-it
//
//  Created by engincankurt on 14/12/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Alamofire
import AVFoundation
import Foundation


class ProfilePageViewController: UIViewController, AVAudioPlayerDelegate, AVAudioRecorderDelegate
{
    @IBOutlet weak var profilePic: UIImageView!

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var recordButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    @IBAction func recordButtonPressed(_ sender: Any) {
    }

    @IBAction func stopButtonPressed(_ sender: Any) {
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
