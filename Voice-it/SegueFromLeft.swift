//
//  SegueFromLeft.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit

class SegueFromLeft: UIStoryboardSegue {
    
    override func perform()
        {
            let src = self.source
            let dst = self.destination
    
            src.view.superview?.insertSubview(dst.view, belowSubview: src.view)
            dst.view.transform = CGAffineTransform(translationX: 0, y: 0)
    
                UIView.animate(withDuration: 0.25,
                    delay: 0.0,
                    options: UIViewAnimationOptions(),
                    animations: {
                    src.view.transform = CGAffineTransform(translationX: src.view.frame.size.width, y: 0)
                },
                        completion: { finished in
                            src.present(dst, animated: false, completion: {
                                src.view.superview?.superview?.insertSubview(dst.view, aboveSubview: src.view)
                            })
                    }
                )
        }
}


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
