//
//  AppData.swift
//  Voice-it
//
//  Created by engincankurt on 27/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit

class AppData: NSObject {
    //Constants
    let userLoginTokenConstantKey:String = "token"
    let usernameConstantKey:String = "username"
    
    //Singleton Object
    static let sharedAppData = AppData()
    
    //Singleton Methods
    func setUserLoginToken(_ token: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(token, forKey: userLoginTokenConstantKey)
    }
    
    func getUserLoginToken() -> String? {
        let userDefaults = UserDefaults.standard
        if let token = userDefaults.string(forKey: userLoginTokenConstantKey){
            print("In getUserLoginToken() token: \(token)")
            return token
        }
        return nil
    }
    
    func setUsername(_ username: String) {
        let userDefaults = UserDefaults.standard
        userDefaults.set(username, forKey: usernameConstantKey)
    }
    
    func getUsername() -> String? {
        let userDefaults = UserDefaults.standard
        if let username = userDefaults.string(forKey: usernameConstantKey){
            print("In getUsername() username: \(username)")
            return username
        }
        return nil
    }
    
    func isLoggedIn() -> Bool {
        let token = getUserLoginToken()
        if(token != nil){
            return true
        }
        return false
    }
    
    func clearData() {
        //make nil the userLoginToken
        let userDefaults = UserDefaults.standard
        userDefaults.removeObject(forKey: userLoginTokenConstantKey)
        userDefaults.removeObject(forKey: usernameConstantKey)
    }
}
