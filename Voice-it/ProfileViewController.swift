//
//  ProfileViewController.swift
//  Voice-it
//
//  Created by engincankurt on 01/11/2016.
//  Copyright © 2016 engincankurt. All rights reserved.
//

import UIKit
import Segmentio
import Alamofire

class ProfileViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var profileView: UIView!
    @IBOutlet weak var inboxTableView: UITableView!
    @IBOutlet weak var outboxTableView: UITableView!
    
    var segmentioView: Segmentio!
    var selectedSegment : Int = 0
    
    var inboxUsersNames = Array(repeating: Array(repeating: "", count: 1), count: 10)
    var messageDate = Array(repeating: Array(repeating: "", count: 1), count: 10)
    
    var outboxDevices = Array(repeating: Array(repeating: "", count: 1), count: 100)
    var messageStatus = Array(repeating: Array(repeating: "", count: 1), count: 100)
    var outboxUsersNames = Array(repeating: Array(repeating: "", count: 1), count: 100)
    
    var totalInboxMessages = 0
    var totalOutboxMessages = 0
    
    let headers: HTTPHeaders = ["Authorization": "Token 393da8d2ac7183fd3a3eb781ad2cbb8f992b7cb8"]

    override func viewDidLoad() {
        super.viewDidLoad()
        var content = [SegmentioItem]()

        let segmentioViewRect = CGRect(x: 0, y:68, width: UIScreen.main.bounds.width, height: 75)
        segmentioView = Segmentio(frame: segmentioViewRect)
        view.addSubview(segmentioView)
        segmentioView.selectedSegmentioIndex = 0
        self.inboxTableView.isHidden = false
        self.loadInboxData()
        self.outboxTableView.isHidden = true
        self.profileView.isHidden = true
        
        let indicatorOptions = SegmentioIndicatorOptions(
            type: .bottom,
            ratio: 1,
            height: 5,
            color: hexStringToUIColor(hex: "FBC33B")
        )
        
        let horizontalSeperatorOptions = SegmentioHorizontalSeparatorOptions(
            type: SegmentioHorizontalSeparatorType.bottom, // Top, Bottom, TopAndBottom
            height: 1,
            color: hexStringToUIColor(hex: "FBC33B")
        )
        
       let verticalSeperatorOptions = SegmentioVerticalSeparatorOptions(
            ratio: 0.6, // from 0.1 to 1
            color: hexStringToUIColor(hex: "FBC33B")
        )
        
        let defaultState = SegmentioState(
            backgroundColor:  hexStringToUIColor(hex: "1B1464"),
            titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
            titleTextColor: UIColor.white
        )
        
        let selectState = SegmentioState(
            backgroundColor: hexStringToUIColor(hex: "1B1464"),
            titleFont: UIFont.systemFont(ofSize: UIFont.smallSystemFontSize),
            titleTextColor: UIColor.white
        )
        
        let highlightedState = SegmentioState(
            backgroundColor: hexStringToUIColor(hex: "1B1464"),
            titleFont: UIFont.boldSystemFont(ofSize: UIFont.smallSystemFontSize),
            titleTextColor: UIColor.white
        )
        
        let segmentioStates = SegmentioStates(
            defaultState: defaultState,
            selectedState: selectState,
            highlightedState: highlightedState
        )
        
        let options = SegmentioOptions(
            backgroundColor: hexStringToUIColor(hex: "1B1464"),
            maxVisibleItems: 3,
            scrollEnabled: true,
            indicatorOptions: indicatorOptions,
            horizontalSeparatorOptions: horizontalSeperatorOptions,
            verticalSeparatorOptions: verticalSeperatorOptions,
            imageContentMode: UIViewContentMode.scaleAspectFit,
            labelTextAlignment: NSTextAlignment.center,
            labelTextNumberOfLines: 1,
            segmentStates: segmentioStates,  // tuple of SegmentioState (defaultState, selectState, highlightedState)
            animationDuration: 0.1
        )
        
        let inbox = SegmentioItem(
            title: "Inbox",
            image: UIImage(named: "Inbox-48")
        )
        
        let outbox = SegmentioItem(
            title: "Outbox",
            image: UIImage(named: "Outbox-48")
        )
        
        let profile = SegmentioItem(
            title: "Profile",
            image: UIImage(named: "User-48")
        )
        
        content.append(inbox)
        content.append(outbox)
        content.append(profile)
        
        segmentioView.setup(
            content: content,
            style: SegmentioStyle.imageOverLabel,
            options: options
        )
        
        segmentioView.valueDidChange = { segmentio, segmentIndex in
            switch segmentIndex{
            case 0:
                self.selectedSegment = segmentIndex
                self.inboxTableView.isHidden = false
                self.loadInboxData()
                self.outboxTableView.isHidden = true
                self.profileView.isHidden = true
                
            case 1:
                self.selectedSegment = segmentIndex
                self.inboxTableView.isHidden = true
                self.outboxTableView.isHidden = false
                self.loadOutboxData()
                self.profileView.isHidden = true
                
            case 2:
                self.selectedSegment = segmentIndex
                self.inboxTableView.isHidden = true
                self.outboxTableView.isHidden = true
                self.profileView.isHidden = false
                
            default:
                break
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        if(self.selectedSegment==0){
            return 1
        }else{
            if(self.selectedSegment==1){
                return 1
            }else{
                return 0
            }
        }
    }
    
    func loadInboxData(){
        var inboxUrl = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages/inbox/"
        inboxUrl.append("3")
        
        Alamofire.request(inboxUrl, method: .get , parameters: nil, encoding: JSONEncoding.default, headers: self.headers) .responseJSON { response in
            
            switch(response.result) {
            case .success(let data):
                let json = JSON(data)
                print(json)
                var countInboxMessages = 0
                self.totalInboxMessages = json.count
                
                while self.totalInboxMessages > countInboxMessages {
                    
                    self.messageDate[countInboxMessages][0] = json[countInboxMessages]["sent_date"].stringValue
                    
                    self.inboxUsersNames[countInboxMessages][0] = json[countInboxMessages]["from_account"]["username"].stringValue
                    
                    self.addElementToInbox()
                    
                    countInboxMessages += 1
                }
                
                print(self.messageDate.description)
                print(self.inboxUsersNames.description)
                
                
            case .failure(_):
                print(response.result.error!)
                break
            }
        }
    }
    
    func loadOutboxData(){
    
        var outboxUrl = "http://voiceit-api-dev.eu-central-1.elasticbeanstalk.com/voice-messages/outbox/"
        outboxUrl.append("4")
        
        Alamofire.request(outboxUrl, method: .get , parameters: nil, encoding: JSONEncoding.default, headers: self.headers) .responseJSON { response in
            
            switch(response.result) {
            case .success(let data):
                let json = JSON(data)
                print(json)
                var countOutboxMessages = 0
                self.totalOutboxMessages = json.count
                
                while self.totalOutboxMessages > countOutboxMessages {
                    self.outboxDevices[countOutboxMessages][0] = json[countOutboxMessages]["to_device"]["name"].stringValue
                    
                    if json[countOutboxMessages]["read_date"].stringValue == "" {
                        self.messageStatus[countOutboxMessages][0] = "Waiting"
                    }else{
                        self.messageStatus[countOutboxMessages][0] = "Received"
                    }
                    
                    self.outboxUsersNames[countOutboxMessages][0] = json[countOutboxMessages]["to_account"]["username"].stringValue
                    
                    self.addElementToOutbox()
                    
                    countOutboxMessages += 1
                }
                
            case .failure(_):
                print(response.result.error!)
                break
            }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(self.selectedSegment==0){
            return self.totalInboxMessages
        }else{
            if(self.selectedSegment==1){
                return self.totalOutboxMessages
            }else{
                return 0
            }
        }
    }
    
    func addElementToInbox() {
        self.inboxTableView.reloadData()
    }
    
    func addElementToOutbox() {
        self.outboxTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(self.selectedSegment==0){
        
            let cell = tableView.dequeueReusableCell(withIdentifier: "InboxCell", for: indexPath) as UITableViewCell
        
            guard let from = cell.viewWithTag(1) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let username = cell.viewWithTag(2) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let date = cell.viewWithTag(3) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let voice = cell.viewWithTag(4) as? UIImageView else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            var dateVal : String = ""
            dateVal = self.messageDate[indexPath.row][0]
            let index1 = dateVal.index(dateVal.startIndex, offsetBy: 16)
            dateVal = dateVal.substring(to: index1)
            dateVal = dateVal.replacingOccurrences(of: "T", with: " ", options: .literal, range: nil)

            from.text = "From"
            username.text = self.inboxUsersNames[indexPath.row][0]
            date.text = dateVal
            voice.image = UIImage(named: "Voice Presentation-96.png")
        
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "OutboxCell", for: indexPath) as UITableViewCell
            
            guard let from = cell.viewWithTag(1) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let deviceName = cell.viewWithTag(2) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let to = cell.viewWithTag(3) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let username = cell.viewWithTag(4) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let status = cell.viewWithTag(5) as? UILabel else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            guard let voice = cell.viewWithTag(6) as? UIImageView else {
                print("ERROR: View with tag 1 is not the type it is casted for.")
                return cell
            }
            
            
            from.text = "From"
            deviceName.text = self.outboxDevices[indexPath.row][0]
            to.text = "to"
            username.text = self.outboxUsersNames[indexPath.row][0]
            status.text = self.messageStatus[indexPath.row][0]
            voice.image = UIImage(named: "Voice Presentation-96.png")
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
    }

    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if(self.selectedSegment==0){
        
        let replyAction = UITableViewRowAction(style: .normal, title: " Reply ") { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
            
            let replyMessage = self.inboxUsersNames[indexPath.row]
            
            let activityViewController = UIActivityViewController(activityItems: [replyMessage], applicationActivities: nil)
            
            self.present(activityViewController, animated: true, completion: nil)
            
            
            //reply
        }
            
        replyAction.backgroundColor  = hexStringToUIColor (hex: "FBC33B")

        return [replyAction]
            
        }else{
            
            if(self.messageStatus[indexPath.row][0] == "Received"){
            
            let repeatAction = UITableViewRowAction(style: .normal, title: " Repeat ") { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
                
                let repeatMessage = self.outboxUsersNames[indexPath.row]
                
                let activityViewController = UIActivityViewController(activityItems: [repeatMessage], applicationActivities: nil)
                
                self.present(activityViewController, animated: true, completion: nil)
                
                //repeat

            }
            
            repeatAction.backgroundColor = hexStringToUIColor (hex: "FBC33B")
            
            return [repeatAction]
                
            }else{
                
                let deleteAction = UITableViewRowAction(style: .normal, title: " Delete ") { (action: UITableViewRowAction!, indexPath: IndexPath!) -> Void in
                    
                    let deleteMessage = self.outboxUsersNames[indexPath.row]
                    
                    let activityViewController = UIActivityViewController(activityItems: [deleteMessage], applicationActivities: nil)
                    
                    self.present(activityViewController, animated: true, completion: nil)
                    
                    
                    //delete

                }
                
            deleteAction.backgroundColor = hexStringToUIColor (hex: "FBC33B")
                
            return [deleteAction]
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35.0
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    

}
